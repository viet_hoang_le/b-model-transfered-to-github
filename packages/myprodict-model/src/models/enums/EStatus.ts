
export const enum EStatus {
  Pending = 1,
  Processing,
  Rejected,
  Active,
  Disabled,
}
