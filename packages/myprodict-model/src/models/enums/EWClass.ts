
// Dictionary word class
export const enum EWClass {
  all = 1,
  noun,
  verb,
  adjective,
  adverb,
  pronoun,
  abbreviation,
  interjection,
  conjunction,
  prefix,
  preposition,
  article,  // DEFINITE AND INDEFINITE ARTICLES: a, an, the
  unclassified,
}
